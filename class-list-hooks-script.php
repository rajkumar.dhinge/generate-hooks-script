<?php
/**
 * Main class for list hooks
 */


//Phase 1 Accessed directly.
//defined( 'ABSPATH' ) || exit;

/**
 * Main hooks list class
 */
class List_Hooks_Script {
	/**
	 * Directory path to csv file.
	 *
	 * @var string
	 */
	public $csv_file_path;
	/**
	 * Directory path to plugin
	 *
	 * @var string
	 */
	public $dir;
	/**
	 * For holding instance of create hook csv class
	 *
	 * @var object
	 */
	public $create_csv_class_instance;
	/**
	 * Stores all hooks data from provided directory
	 *
	 * @var array
	 */
	public $all_hooks_data = array();
	/**
	 * Constructor
	 */
	public function __construct( $dir, $csv_file_path ) {
		$this->dir           = $dir;
		$this->csv_file_path = $csv_file_path;
		require 'class-create-hooks-csv.php';
		$this->create_csv_class_instance = Create_Hooks_Csv::instance();
		if ( is_dir( $this->dir ) ) {
			$this->get_file_paths_for_reading();
		} else {
			echo "<meta http-equiv=\"refresh\" content=\"3;url=index.html\" />";
			echo "<style>
			@import compass/css3;
			
			/* ===================== FILE INPUT ===================== */
			.file-area {
			  width: 100%;
			  position: relative;
			  
			  input[type=file] {
				position: absolute;
				width: 100%;
				height: 100%;
				top: 0;
				left: 0;
				right: 0;
				bottom: 0;
				opacity: 0;
				cursor: pointer;
			  }
			  
			  .file-dummy {
				width: 100%;
				padding: 30px;
				background: rgba(255,255,255,0.2);
				border: 2px dashed rgba(255,255,255,0.2);
				text-align: center;
				transition: background 0.3s ease-in-out;
				
				.success {
				  display: none;
				}
			  }
			  
			  &:hover .file-dummy {
				background: rgba(255,255,255,0.1);
			  }
			  
			  input[type=file]:focus + .file-dummy {
				outline: 2px solid rgba(255,255,255,0.5);
				outline: -webkit-focus-ring-color auto 5px;
			  }
			  
			  input[type=file]:valid + .file-dummy {
				border-color: rgba(0,255,0,0.4);
				background-color: rgba(0,255,0,0.3);
			
				.success {
				  display: inline-block;
				}
				.default {
				  display: none;
				}
			  }
			}
			
			
			/* ===================== BASIC STYLING ===================== */
			
			* {
			  box-sizing: border-box;
			  font-family: 'Lato', sans-serif;
			}
			
			html,
			body {
			  margin: 0;
			  padding: 0;
			  font-weight: 300;
			  height: 100%;
			  background: #053777;
			  color: #fff;
			  font-size: 16px;
			  overflow: hidden;
			  background: -moz-linear-gradient(top, #053777 0%, #00659b 100%);
			  /* FF3.6+ */
			  
			  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #053777), color-stop(100%, #00659b));
			  /* Chrome,Safari4+ */
			  
			  background: -webkit-linear-gradient(top, #053777 0%, #00659b 100%);
			  /* Chrome10+,Safari5.1+ */
			  
			  background: -o-linear-gradient(top, #053777 0%, #00659b 100%);
			  /* Opera 11.10+ */
			  
			  background: -ms-linear-gradient(top, #053777 0%, #00659b 100%);
			  /* IE10+ */
			  
			  background: linear-gradient(to bottom, #053777 0%, #00659b 100%);
			  /* W3C */
			  
			}
			
			h1 {
			  text-align: center;
			  margin: 50px auto;
			  font-weight: 100;
			}
			
			label {
			  font-weight: 500;
			  display: block;
			  margin: 4px 0;
			  text-transform: uppercase;
			  font-size: 13px;
			  overflow: hidden;
			  
			  span {
				float: right;
				text-transform: none;
				font-weight: 200;
				line-height: 1em;
				font-style: italic;
				opacity: 0.8;
			  }
			}
			
			.form-controll {
			  display: block;
			  padding: 8px 16px;
			  width: 100%;
			  font-size: 16px;
			  background-color: rgba(255,255,255,0.2);
			  border: 1px solid rgba(255,255,255,0.3);
			  color: #fff;
			  font-weight: 200;
			  
			  &:focus {
				outline: 2px solid rgba(255,255,255,0.5);
				outline: -webkit-focus-ring-color auto 5px;
			  }
			}
			
			button {
			  padding: 8px 30px;
			  background: rgba(255,255,255,0.8);
			  color: #053777;
			  text-transform: uppercase;
			  font-weight: 600;
			  font-size: 11px;
			  border: 0;
			  text-shadow: 0 1px 2px #fff;
			  cursor: pointer;
			}
			
			.form-group {
			  max-width: 500px;
			  margin: auto;
			  margin-bottom: 30px;
			}
			
			.back-to-article {
			  color: #fff;
			  text-transform: uppercase;
			  font-size: 12px;
			  position: absolute;
			  right: 20px;
			  top: 20px;
			  text-decoration: none;
			  display: inline-block;
			  background: rgba(0,0,0,0.6);
			  padding: 10px 18px;
			  transition: all 0.3s ease-in-out;
			  opacity: 0.6;
			  
			  &:hover {
				opacity: 1;
				background: rgba(0,0,0,0.8);
			  }
			</style>";
			echo "<center><h1>Redirecting back : Invalid plugin zip file.</center>";
			$filename = basename($_FILES["fileToUpload"]["name"]);
			$without_extension = basename('temp/'.$filename, '.zip');
			$this->deleteDir('temp/'.$without_extension);
			unlink("temp/".$filename);
	  
			exit;
		}
	}
	public function deleteDir($dir) {
		if (!file_exists($dir)) {
			return true;
		}
	  
		if (!is_dir($dir)) {
			return unlink($dir);
		}
	  
		foreach (scandir($dir) as $item) {
			if ($item == '.' || $item == '..') {
				continue;
			}
	  
			if (!$this->deleteDir($dir . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
	  
		}
	  
		return rmdir($dir);
	  }
	/**
	 * This function opens csv file in specified mode
	 *
	 * @param  string $csv_file_path    csv file path to be opened.
	 * @param  string $mode             mode in which file should be opened.
	 * @return                          opened file pointer/false
	 */
	public static function open_csv_file( $csv_file_path, $mode ) {
			return fopen( $csv_file_path, $mode );
	}
	/**
	 * Gets all files and their paths
	 */
	public function get_file_paths_for_reading() {

		$file_paths = new RecursiveTreeIterator( new RecursiveDirectoryIterator( $this->dir, RecursiveDirectoryIterator::SKIP_DOTS ) );
		$file       = self::open_csv_file( $this->csv_file_path, 'w' );
		if ( false !== $file ) {
			fputcsv( $file, array( 'Hooks Name', 'Arguments', 'Type', 'Hook Description', 'Source' ) );
			fclose( $file );
		}

		foreach ( $file_paths as $hook_file_pathpath ) {
			if ( strpos( $hook_file_pathpath, '.php' ) !== false ) {
				$hook_file_pathpath   = substr( $hook_file_pathpath, strpos( $hook_file_pathpath, '/' ) );
				$sss                  = $this->read_file_for_hook( $hook_file_pathpath, $this->csv_file_path );
				$this->all_hooks_data = array_merge( $this->all_hooks_data, $sss );
			}
		}
		$this->create_csv_class_instance->create_csv_for_hook_desc( $this->all_hooks_data, $this->csv_file_path );
	}
	/**
	 * Reads given file line by line and finds out hooks from them.
	 *
	 * @param  string $hook_file_path  Path to php file to be read.
	 */
	public function read_file_for_hook( $hook_file_path ) {
		$file                      = fopen( $hook_file_path, 'r' );
		$line_no                   = 0;
		$hooks_data_of_single_file = array();
		while ( ! feof( $file ) ) {
			$line_no++;
			$file_line              = fgets( $file );
			$hook_string_content    = '';
			$type                   = '';
			$description_comment    = '';
			$parameters_description = '';
			if ( strpos( $file_line, '/**' ) ) {
				$comment_line_part;
				while ( ! feof( $file ) ) {
					$comment_line_part = fgets( $file );
					$line_no++;
					if ( strpos( $comment_line_part, '*/' ) ) {
						break;
					}
					if ( strpos( $comment_line_part, '*' ) ) {
							$comment_line_part = trim( $comment_line_part, "\n" );
						if ( strpos( $comment_line_part, '@param' ) ) {
							// To remove unwanted string part.
							$comment_line_part    = substr( $comment_line_part, strpos( $comment_line_part, '@param' ) + 7 );
							$comment_line_part    = trim( $comment_line_part );
							$temp_parameter_line  = explode( ' ', $comment_line_part );
							$explode_array_length = count( $temp_parameter_line );
							// parameter has atleast some string to display.
							if ( 1 < $explode_array_length ) {
								$parameters_description .= '<p>' . $temp_parameter_line[0] . $temp_parameter_line[1] . ': ';
								for ( $i = 2; $i < $explode_array_length; $i++ ) {
									if ( '' !== $temp_parameter_line[ $i ] ) {
										$parameters_description .= $temp_parameter_line[ $i ] . ' ';
									}
								}
								$parameters_description .= '</p>';
							}
						}
						if ( ! strpos( $comment_line_part, '@' ) ) {
							$comment_line_part = str_replace( '*', '', $comment_line_part );
							$comment_line_part = trim( trim( $comment_line_part, "\t" ), ' ' );
								// avoid empty lines.
							if ( '' !== $comment_line_part ) {
								$description_comment = $description_comment . $comment_line_part;
							}
						}
					}
				}
				$file_line = fgets( $file );
				$line_no++;
			}
			$action_position = strpos( $file_line, 'do_action(' );
			$filter_position = strpos( $file_line, 'apply_filters(' );
			if ( false !== $action_position && ( '' !== $description_comment ) ) {
				// If current line contains do_action.
				$end_pointer_of_hook = strpos( $file_line, ');' );
				$action_string       = substr( $file_line, $action_position );
				$action_string       = substr( $action_string, strpos( $action_string, '(' ) );
				$hook_string_content = $action_string;
				$type                = 'Action';

			} elseif ( false !== $filter_position && ( '' !== $description_comment ) ) {
				$end_pointer_of_hook = strpos( $file_line, ');' );
				$filter_string       = substr( $file_line, $filter_position );
				$filter_string       = substr( $filter_string, strpos( $filter_string, '(' ) );
				$hook_string_content = $filter_string;
				$type                = 'Filter';
			}
			// Enters If current readed line has hook either do_action or apply_filters.
			if ( '' !== $hook_string_content && ( ! strpos( $hook_string_content, 'str_replace(' ) ) ) {
				$increment_line_no = 0;
				if ( 2 >= strlen( $hook_string_content ) ) {

					$hook_string_content = $hook_string_content . fgets( $file );
					$increment_line_no++;
					if ( strpos( $hook_string_content, ',' ) ) {
						$increment_line_no++;
						$hook_string_content = $hook_string_content . fgets( $file );
						// The reason for using end of file is bcoz we don't know how many arguments have given and how many lines have taken for hook.
						while ( ! feof( $file ) ) {
							if ( substr_count( $hook_string_content, '(' ) === substr_count( $hook_string_content, ')' ) ) {
								break;
							}
							$increment_line_no++;
							$hook_string_content = $hook_string_content . fgets( $file );
						}
					}
				}
				$hook_content = array();

				// To find start point of parameters.
				$parameter_position = strpos( $hook_string_content, ',' );
				if ( false !== $parameter_position ) {
					// pointer is here means hook has atleast one parameter.
					$hook_content['name']      = trim( substr( $hook_string_content, 1, strpos( $hook_string_content, ',' ) ), ',' );
					$hook_content['arguments'] = $parameters_description;
				} else {
					$hook_content['name']      = trim( substr( $hook_string_content, strpos( $hook_string_content, '(' ) + 1, strpos( $hook_string_content, ')' ) - strpos( $hook_string_content, '(' ) ), ')' );
					$hook_content['arguments'] = 'No Arguments';
				}
				$hook_content['type']        = $type;
				$hook_content['description'] = $description_comment;
				$temp_hook_path              = explode( '/', $this->dir );
				$temp_hook_path              = array_reverse( $temp_hook_path );
				if ( '' === $temp_hook_path[0] ) {
					$hook_content['source'] = '<p>' . substr( $hook_file_path, strpos( $hook_file_path, $temp_hook_path[1] ) ) . ': ' . $line_no . '</p>';
				} else {
					$hook_content['source'] = '<p>' . substr( $hook_file_path, strpos( $hook_file_path, $temp_hook_path[0] ) ) . ': ' . $line_no . '</p>';
				}

				$hook_content['name'] = trim( str_replace( array( "\r", "\n" ), '', $hook_content['name'] ) );
				// to ensure there is no quotes in hook name.
				$hook_content['name'] = trim( $hook_content['name'], "'" );
				$hook_content['name'] = trim( $hook_content['name'], '"' );
				$existing_hook_key    = null;
				$existing_hook_key    = array_search( $hook_content['name'], array_column( $this->all_hooks_data, 'name' ), true );
				// This part is for duplicate hooks.
				if ( ( false === $existing_hook_key ) && ( false === array_search( $hook_content['name'], array_column( $hooks_data_of_single_file, 'name' ), true ) ) ) {
					$hooks_data_of_single_file[] = $hook_content;
				} else {
					if ( $hook_content['name'] == $this->all_hooks_data[ $existing_hook_key ]['name'] ) {
					$this->all_hooks_data[ $existing_hook_key ]['source'] = $this->all_hooks_data[ $existing_hook_key ]['source'] . $hook_content['source'];
					}
				}
				$line_no += $increment_line_no;
			}
		}
		fclose( $file );
		return $hooks_data_of_single_file;
	}
}

