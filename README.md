# Generate hooks script

![gui](/uploads/9b6f3d7388387f458244d01a4e67f2f5/gui.png)

Done:
* GUI Added.
* Client side validations on GUI added.
* Server side validations added in PHP.
* Integrated with the hooks generation files.
* CSV Download Script created.
* Duplicate/incorrects source files for hooks.
* Tested incorrect hooks issue.
* Issue fixed: Extra Space added at start line in CSV file.
* Proper file deletion handling.


TO-DO / Known Issues:
* If more than one duplicate hooks are present in same file only first instance would be captured.

Optimization:
* Implementaion in OOP.
* Cleaning Code.
* Adding Comments.
* Testing.
