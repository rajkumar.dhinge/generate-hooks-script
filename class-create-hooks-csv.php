<?php
/**
 * Creates csv for hook description
 *
 * @package listHooks
 */

// Phase 1 accessed directly.
//defined( 'ABSPATH' ) || exit;

/**
 * Displays hooks table class
 */
class Create_Hooks_Csv {
	/**
	 * The single instance of the class.
	 *
	 * @var instance
	 */
	protected static $instance = null;
	/**
	 *
	 * To create Instance of class.
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	/**
	 * Constructor
	 */
	public function __construct() {
	}
	/**
	 * This function creates csv for hook description.
	 *
	 * @param  array  $hook_content   array of hook data.
	 * @param  string $csv_file_path  csv file path.
	 */
	public function create_csv_for_hook_desc( $hook_content, $csv_file_path ) {
		$file = List_Hooks_Script::open_csv_file( $csv_file_path, 'a+' );
		if ( false !== $file ) {
			foreach ( $hook_content as $key => $value ) {
				fputcsv( $file, $value );
			}
			fclose( $file );
		}
	}
}
