<?php
if(isset($_POST['title']) && isset($_FILES["fileToUpload"]["name"])){
  //Variable initializ
  $target_dir = "temp/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
  $filename = basename($_FILES["fileToUpload"]["name"]);
  $without_extension = basename($filename, '.zip');
  $temp = str_replace("\\","/",dirname(__FILE__));
  $in = $temp."/temp/".$without_extension;
  $out = $temp."/temp/".$_POST['title'].".csv";
  $file = "temp";
  if (!file_exists($file)) {
    mkdir("temp");
  }
  // Check if file already exists
  if (file_exists($target_file) && $target_file!='temp/') {
    if(!deleteDir('temp/'.$without_extension)){
    $uploadOk = 0; 
    }
  }

  // Check file size
  if ($_FILES["fileToUpload"]["size"] > 20000000) {
    $uploadOk = 0;
  }

  // Allow certain file formats
  if($imageFileType != "zip" ) {
    $uploadOk = 0;
  }

  // Check if $uploadOk is set to 0 by an error
  if ($uploadOk == 0) {
  // if everything is ok, try to upload file
  } else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      // assuming file.zip is in the same directory as the executing script.
      $file = $target_file;

      // get the absolute path to $file
      $path = pathinfo(realpath($file), PATHINFO_DIRNAME);

      $zip = new ZipArchive;
      $res = $zip->open($file);
      if ($res === TRUE) {
        // extract it to the path we determined above
        $zip->extractTo($path);
        $zip->close();
  
        include "class-list-hooks-script.php";
        new List_Hooks_Script( $in , $out );
  
        if(deleteDir('temp/'.$without_extension)){
          if(unlink("temp/".$filename)){
            ob_clean();
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="'.$_POST['title'].'.csv"');
            header("Content-Length: ".filesize($out));
            readfile( $out ); 
            sleep( 7 );
            unlink($out);
          }else{
          }
        }

      } else {
        echo "Doh! I couldn't open $file";
  }
    } else {
      echo "Sorry, there was an error uploading your file.";
    }
}
}else{
  header('Location: index.html');
  exit;
}
function deleteDir($dir) {
  if (!file_exists($dir)) {
      return true;
  }

  if (!is_dir($dir)) {
      return unlink($dir);
  }

  foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
          continue;
      }

      if (!deleteDir($dir . DIRECTORY_SEPARATOR . $item)) {
          return false;
      }

  }

  return rmdir($dir);
}
?>
